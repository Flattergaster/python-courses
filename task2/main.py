def gen(tup):
    return {key: value for item in tup for key, value in item.items()}


def split(s, sep):
    start = 0
    while True:
        sub = s.find(sep, start)
        if sub == -1:
            yield s[start:]
            break
        yield s[start:sub]
        start = sub + len(sep)


def to_dict_recursive(items):
    res = {}
    for item in items:
        parse(res, item)
    return res


def parse(res, item):
    sep = item.find('.')
    if sep != -1:
        dest = item[:sep]
        if not dest in res.keys():
            res[dest] = {}
        return parse(res[dest], item[sep + 1:])
    if len(item) > 0:
        res[item] = {}
        return res


def to_dict(items):
    res = {}
    for item in items:
        dest = res
        for part in split(item, '.'):
            if not part in dest:
                dest[part] = {}
            dest = dest[part]
    return res

if __name__ == "__main__":

    data = (
        {'qwe': 1},
        {'asd': 2},
        {'zxc': 3},
    )
    print(gen(data))
    
    columns = (
        'person.surname',
        'person.firstname',
        'person.patronymic',
        'person.documents.type',
        'person.documents.series',
        'person.documents.number',
        'person.employee_set.unit.short_name',
        'person.employee_set.job.name',
        'person.employee_set.employment_date',
        'person.employee_set.dismissal_date',
        'person.employee_set.group_set.name',
        'person.employee_set.group_set.period.code',
        'unit.short_name',
    )

    for i in split(columns[11], "."):
        print(i)

    rec = to_dict_recursive(columns)
    it = to_dict(columns)
    print(it)
    print(rec)
    assert rec == it
