def func(n):
    assert isinstance(n, int), type(n)
    assert n > 0
    result = []
    if n % 3 == 0:
        result.append('foo')
    if n % 5 == 0:
        result.append('bar')
    return ' '.join(result) if result else str(n)

if __name__ == '__main__':
    assert func(15) == 'foo bar'
    assert func(25) == 'bar'
    assert func(24) == 'foo'
    assert func(17) == '17'
